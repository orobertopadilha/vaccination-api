package br.edu.unisep.vaccination.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class DefaultErrorDto {

    private String message;
}
