package br.edu.unisep.vaccination.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class VaccinationDto {

    private Integer id;

    private String patientName;
    private String patientPhone;

    private Integer vaccineTypeId;
    private String vaccineType;

    private LocalDate firstDosage;
    private LocalDate prevSecondDosage;
    private LocalDate actualSecondDosage;

    private Integer statusId;
    private String status;

}
