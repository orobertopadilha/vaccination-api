package br.edu.unisep.vaccination.domain.dto;

import lombok.Data;

@Data
public class RegisterVaccinationDto {

    private String patientName;
    private String patientPhone;

    private Integer vaccineTypeId;

}
