package br.edu.unisep.vaccination.exception;

import br.edu.unisep.vaccination.domain.dto.DefaultErrorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler({IllegalArgumentException.class, NullPointerException.class})
    public ResponseEntity<DefaultErrorDto> handleValidationErrors(Exception exception) {
        return ResponseEntity.badRequest().body(new DefaultErrorDto(exception.getMessage()));
    }

}
